package com.loveholidays.addressfinder.controllers;

import com.loveholidays.addressfinder.domain.Addresses;
import com.loveholidays.addressfinder.api.GetAddressAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import retrofit2.Response;

import java.io.IOException;

import static org.apache.commons.lang.StringUtils.isBlank;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Controller
public class HomePageController {

    static final String ERROR_MESSAGE = "errorMessage";
    private final GetAddressAPI addressAPI;
    private final String apiKey;

    @Autowired
    public HomePageController(GetAddressAPI addressAPI, @Value("${getAddress.api.key}") String apiKey) {
        this.addressAPI = addressAPI;
        this.apiKey = apiKey;
    }

    @GetMapping("/" )
    public ModelAndView showHomePage(@RequestParam(value = "postCode", required = false) String postCode) {
        if(isBlank(postCode)){
            return new ModelAndView("index");
        }
        try {
            final Response<Addresses> response = addressAPI.searchAddresses(postCode, apiKey).execute();
            return response.isSuccessful()?
                    new ModelAndView("index", "addresses", response.body()) : new ModelAndView("index", ERROR_MESSAGE, errorMessageForResponse(response, postCode));
        } catch (IOException e) {
            return new ModelAndView("index", ERROR_MESSAGE, "Some error has occurred while trying to get addresses. Please contact support");
        }
    }

    private static String errorMessageForResponse(Response<Addresses> response, String postCode) {
        if(response.code() == NOT_FOUND.value()){
            return String.format("Could not find addresses for postcode [%s]", postCode);
        }else if(response.code() == BAD_REQUEST.value()){
            return String.format("Invalid postcode [%s]", postCode);
        }
        return String.format("Some error has occurred while trying to get addresses. Please contact support");
    }

}

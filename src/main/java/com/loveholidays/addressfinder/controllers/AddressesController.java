package com.loveholidays.addressfinder.controllers;

import com.loveholidays.addressfinder.api.GetAddressAPI;
import com.loveholidays.addressfinder.domain.Addresses;
import com.loveholidays.addressfinder.domain.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import retrofit2.Response;

import java.io.IOException;

import static org.apache.commons.lang.StringUtils.isBlank;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.ResponseEntity.*;

@RestController
public class AddressesController {

    static final String INTERNAL_SERVER_ERROR_MESSAGE = "Some error has occurred while trying to get addresses. Please contact support";
    private final GetAddressAPI addressAPI;
    private final String apiKey;

    @Autowired
    public AddressesController(GetAddressAPI addressAPI, @Value("${getAddress.api.key}") String apiKey) {
        this.addressAPI = addressAPI;
        this.apiKey = apiKey;
    }

    @GetMapping("/addresses/{postCode}" )
    public ResponseEntity<?> getAddresses(@PathVariable(value = "postCode", required = false) String postCode) {
        if(isBlank(postCode)){
            return badRequest().body(new ErrorMessage("PostCode is required"));
        }
        try {
            final Response<Addresses> response = addressAPI.searchAddresses(postCode, apiKey).execute();
            return response.isSuccessful()?
                    ok(response.body()) : errorResponse(response, postCode);
        } catch (IOException e) {
               return status(SERVICE_UNAVAILABLE).body(new ErrorMessage("Some error has occurred while trying to get addresses. Please try again later"));
        }
    }

    private static ResponseEntity<ErrorMessage> errorResponse(Response<Addresses> response, String postCode) {
        if(response.code() == NOT_FOUND.value()){
            return status(NOT_FOUND).body(new ErrorMessage(String.format("Could not find addresses for postcode [%s]", postCode)));
        }else if(response.code() == BAD_REQUEST.value()){
            return status(BAD_REQUEST).body(new ErrorMessage(String.format("Invalid postcode [%s]",postCode)));
        }
        return status(INTERNAL_SERVER_ERROR).body(new ErrorMessage(INTERNAL_SERVER_ERROR_MESSAGE));
    }

}

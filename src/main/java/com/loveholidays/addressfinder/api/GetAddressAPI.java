package com.loveholidays.addressfinder.api;


import com.loveholidays.addressfinder.domain.Addresses;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetAddressAPI {

    @GET("/v2/uk/{postCode}")
    Call<Addresses> searchAddresses(@Path("postCode") String postCode, @Query("api-key") String apiKey);
}

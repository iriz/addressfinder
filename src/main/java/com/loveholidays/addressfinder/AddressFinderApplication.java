package com.loveholidays.addressfinder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loveholidays.addressfinder.api.GetAddressAPI;
import okhttp3.OkHttpClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

@SpringBootApplication
@SuppressWarnings("unused")
public class AddressFinderApplication {

	@Bean
	public OkHttpClient okHttpClient() {
		return new OkHttpClient.Builder().build();
	}

	@Bean
	public Retrofit retrofit(OkHttpClient client) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
		return new Retrofit.Builder()
				.addConverterFactory(JacksonConverterFactory.create(objectMapper))
				.baseUrl("https://api.getAddress.io")
				.client(client)
				.build();
	}

    @Bean
    public GetAddressAPI getAddressAPI(Retrofit retrofit){
        return retrofit.create(GetAddressAPI.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(AddressFinderApplication.class, args);
	}
}

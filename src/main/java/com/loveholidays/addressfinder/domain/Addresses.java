package com.loveholidays.addressfinder.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang.StringUtils.split;

public class Addresses {

    @JsonProperty("Latitude")
    private Double latitude;
    @JsonProperty("Longitude")
    private Double longitude;
    @JsonProperty("Addresses")
    private List<String> addresses;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<String> addresses) {
        this.addresses = addresses;
    }

    public List<String> formattedAddresses(){
        if(addresses == null) return emptyList();
        return addresses.stream().filter(StringUtils::isNotBlank).map(Addresses::formattedAddress)
                .collect(toList());
    }

    private static String formattedAddress(String address){
        return Stream.of(split(address,","))
                .map(StringUtils::trim).filter(StringUtils::isNotBlank)
                .collect(joining(", "));
    }
}

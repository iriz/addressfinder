package com.loveholidays.addressfinder.domain;


import com.fasterxml.jackson.annotation.JsonCreator;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import static java.util.Objects.isNull;

public class ErrorMessage {

    private String message;

    @JsonCreator
    public ErrorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (isNull(o) || getClass() != o.getClass()) return false;

        ErrorMessage that = (ErrorMessage) o;

        return new EqualsBuilder().append(this.message, that.message)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(123, 231).append(message)
                .toHashCode();
    }
}

package com.loveholidays.addressfinder;

import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddressFinderApplicationTests {


	@Test(expected = Test.None.class)
	public void shouldLoadContext() {
		//intentionally left blank
	}

}

package com.loveholidays.addressfinder.controllers;

import com.loveholidays.addressfinder.api.GetAddressAPI;
import com.loveholidays.addressfinder.domain.Addresses;
import com.loveholidays.addressfinder.domain.ErrorMessage;
import okhttp3.ResponseBody;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import retrofit2.Call;

import java.io.IOException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.*;
import static retrofit2.Response.error;
import static retrofit2.Response.success;

@RunWith(MockitoJUnitRunner.class)
public class AddressesControllerTest {

    private static final String API_KEY = "some_api_key";
    @Mock
    private GetAddressAPI getAddressAPI;
    @Mock
    private Call<Addresses> call;

    private AddressesController controller;

    @Before
    public void setup(){
        reset(getAddressAPI, call);
        controller = new AddressesController(getAddressAPI, API_KEY);
    }

    @Test
    public void shouldReturnBadRequestGivenPostCodeIsBlank(){
        //when postCode is null
        assertErrorResponse(controller.getAddresses(null), BAD_REQUEST, new ErrorMessage("PostCode is required"));

        //when postCode is spaces
        assertErrorResponse(controller.getAddresses("    "), BAD_REQUEST, new ErrorMessage("PostCode is required"));
    }

    @Test
    public void shouldReturnNotFoundGivenNoAddressesIsFound() throws IOException {
        String postCode = "AA2 BB3";
        when(call.execute()).thenReturn(error(NOT_FOUND.value(), mock(ResponseBody.class)));
        when(getAddressAPI.searchAddresses(postCode, API_KEY)).thenReturn(call);

        ResponseEntity<?> response= controller.getAddresses(postCode);

        assertErrorResponse(response, NOT_FOUND, new ErrorMessage(String.format("Could not find addresses for postcode [%s]", postCode)));
    }

    @Test
    public void shouldReturnBadRequestGivenPostCodeIsInvalid() throws IOException {
        String postCode = "AA2 BB3";
        when(call.execute()).thenReturn(error(BAD_REQUEST.value(), mock(ResponseBody.class)));
        when(getAddressAPI.searchAddresses(postCode, API_KEY)).thenReturn(call);

        ResponseEntity<?> response= controller.getAddresses(postCode);

        assertErrorResponse(response, BAD_REQUEST, new ErrorMessage(String.format("Invalid postcode [%s]", postCode)));
    }


    @Test
    public void shouldReturnInternalServerErrorGivenUnExpectedStatusCodeIsReturned() throws IOException {
        String postCode = "AA2 BB3";
        when(call.execute()).thenReturn(error(FORBIDDEN.value(), mock(ResponseBody.class)));
        when(getAddressAPI.searchAddresses(postCode, API_KEY)).thenReturn(call);

        ResponseEntity<?> response= controller.getAddresses(postCode);

        assertErrorResponse(response, INTERNAL_SERVER_ERROR, new ErrorMessage("Some error has occurred while trying to get addresses. Please contact support"));
    }

    @Test
    public void shouldReturnTemporarilyUnavailbleGivenCallToAddressAPIFails() throws IOException {
        String postCode = "AA2 BB3";
        when(call.execute()).thenThrow(new IOException("some io error"));
        when(getAddressAPI.searchAddresses(postCode, API_KEY)).thenReturn(call);

        ResponseEntity<?> response = controller.getAddresses(postCode);

        assertErrorResponse(response, SERVICE_UNAVAILABLE, new ErrorMessage("Some error has occurred while trying to get addresses. Please try again later"));
    }

    @Test
    public void shouldReturnOkResponseGivenAddressesAreFound() throws IOException {
        String postCode = "AA2 BB3";
        final Addresses body = new Addresses();
        when(call.execute()).thenReturn(success(body));
        when(getAddressAPI.searchAddresses(postCode, API_KEY)).thenReturn(call);

        ResponseEntity<?> response = controller.getAddresses(postCode);

        assertThat(response, is(notNullValue()));
        assertThat(response.getStatusCode(), is(OK));
        assertThat(response.getBody(), instanceOf(Addresses.class));
        assertThat(response.getBody(), is(body));
    }

    private static void assertErrorResponse(ResponseEntity<?> response, HttpStatus expectedStatus, ErrorMessage expectedErrorMessage){
        assertThat(response, is(notNullValue()));
        assertThat(response.getStatusCode(), is(expectedStatus));
        assertThat(response.getBody(), instanceOf(ErrorMessage.class));
        assertThat(response.getBody(), is(expectedErrorMessage));
    }

}
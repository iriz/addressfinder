package com.loveholidays.addressfinder.controllers;

import com.loveholidays.addressfinder.domain.Addresses;
import com.loveholidays.addressfinder.api.GetAddressAPI;
import okhttp3.ResponseBody;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;
import retrofit2.Call;

import java.io.IOException;

import static com.loveholidays.addressfinder.controllers.HomePageController.ERROR_MESSAGE;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.*;
import static retrofit2.Response.error;
import static retrofit2.Response.success;

@RunWith(MockitoJUnitRunner.class)
public class HomePageControllerTest {

    private static final String API_KEY = "some_api_key";
    @Mock
    private GetAddressAPI getAddressAPI;
    @Mock
    private Call<Addresses> call;
    private HomePageController controller;

    @Before
    public void setup(){
        reset(getAddressAPI, call);
        controller = new HomePageController(getAddressAPI, API_KEY);
    }

    @Test
    public void shouldReturnViewGivenPostCodeIsMissing(){
        //when postCode is null
        ModelAndView modelAndView = controller.showHomePage(null);
        assertThat(modelAndView, is(notNullValue()));
        assertThat(modelAndView.getViewName(), is("index"));
        assertTrue(modelAndView.getModelMap().isEmpty());

        //when postCode is spaces
        modelAndView = controller.showHomePage("    ");
        assertThat(modelAndView, is(notNullValue()));
        assertThat(modelAndView.getViewName(), is("index"));
        assertTrue(modelAndView.getModelMap().isEmpty());
    }

    @Test
    public void shouldReturnErrorMessageGivenNoAddressesIsFound() throws IOException {
        String postCode = "AA2 BB3";
        when(call.execute()).thenReturn(error(NOT_FOUND.value(), mock(ResponseBody.class)));
        when(getAddressAPI.searchAddresses(postCode, API_KEY)).thenReturn(call);

        ModelAndView modelAndView = controller.showHomePage(postCode);

        assertThat(modelAndView, is(notNullValue()));
        assertThat(modelAndView.getViewName(), is("index"));
        assertThat(modelAndView.getModel(), not(hasKey("addresses")));
        assertThat(modelAndView.getModelMap().get(ERROR_MESSAGE), is(notNullValue()));
        assertThat(modelAndView.getModelMap().get(ERROR_MESSAGE), is(String.format("Could not find addresses for postcode [%s]", postCode)));
    }


    @Test
    public void shouldReturnErrorMessageGivenPostCodeIsInvalid() throws IOException {
        String postCode = "AA2 BB3";
        when(call.execute()).thenReturn(error(BAD_REQUEST.value(), mock(ResponseBody.class)));
        when(getAddressAPI.searchAddresses(postCode, API_KEY)).thenReturn(call);

        ModelAndView modelAndView = controller.showHomePage(postCode);

        assertThat(modelAndView, is(notNullValue()));
        assertThat(modelAndView.getViewName(), is("index"));
        assertThat(modelAndView.getModel(), not(hasKey("addresses")));
        assertThat(modelAndView.getModelMap().get(ERROR_MESSAGE), is(notNullValue()));
        assertThat(modelAndView.getModelMap().get(ERROR_MESSAGE), is(String.format("Invalid postcode [%s]", postCode)));
    }

    @Test
    public void shouldReturnErrorMessageGivenUnExpectedStatusCodeIsReturned() throws IOException {
        String postCode = "AA2 BB3";
        when(call.execute()).thenReturn(error(FORBIDDEN.value(), mock(ResponseBody.class)));
        when(getAddressAPI.searchAddresses(postCode, API_KEY)).thenReturn(call);

        ModelAndView modelAndView = controller.showHomePage(postCode);

        assertThat(modelAndView, is(notNullValue()));
        assertThat(modelAndView.getViewName(), is("index"));
        assertThat(modelAndView.getModel(), not(hasKey("addresses")));
        assertThat(modelAndView.getModelMap().get(ERROR_MESSAGE), is(notNullValue()));
        assertThat(modelAndView.getModelMap().get(ERROR_MESSAGE), is("Some error has occurred while trying to get addresses. Please contact support"));
    }

    @Test
    public void shouldReturnErrorMessageGivenCallToAddressAPIFails() throws IOException {
        String postCode = "AA2 BB3";
        when(call.execute()).thenThrow(new IOException("some io error"));
        when(getAddressAPI.searchAddresses(postCode, API_KEY)).thenReturn(call);

        ModelAndView modelAndView = controller.showHomePage(postCode);

        assertThat(modelAndView, is(notNullValue()));
        assertThat(modelAndView.getViewName(), is("index"));
        assertThat(modelAndView.getModel(), not(hasKey("addresses")));
        assertThat(modelAndView.getModelMap().get(ERROR_MESSAGE), is(notNullValue()));
        assertThat(modelAndView.getModelMap().get(ERROR_MESSAGE), is("Some error has occurred while trying to get addresses. Please contact support"));
    }

    @Test
    public void shouldReturnAddressesGivenAddressesAreFound() throws IOException {
        String postCode = "AA2 BB3";
        final Addresses body = new Addresses();
        when(call.execute()).thenReturn(success(body));
        when(getAddressAPI.searchAddresses(postCode, API_KEY)).thenReturn(call);

        ModelAndView modelAndView = controller.showHomePage(postCode);

        assertThat(modelAndView, is(notNullValue()));
        assertThat(modelAndView.getViewName(), is("index"));
        assertThat(modelAndView.getModel(), not(hasKey(ERROR_MESSAGE)));
        assertThat(modelAndView.getModel(), hasEntry("addresses", body));
    }
}
package com.loveholidays.addressfinder.api;

import com.loveholidays.addressfinder.domain.Addresses;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import retrofit2.Response;

import java.io.IOException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.springframework.http.HttpStatus.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GetAddressAPITests {

    @Value("${getAddress.api.key}")
    private String apiKey;

    @Autowired
    private GetAddressAPI getAddressAPI;

    @Test
    public void returnsOKResponseGivenPostCodeIsFound() throws IOException {
        final Response<Addresses> response = getAddressAPI.searchAddresses("XX2 00X", apiKey).execute();

        assertThat(response, is(notNullValue()));
        assertThat(response.code(), is(OK.value()));
        assertThat(response.body(), is(notNullValue()));
        assertThat(response.body(), instanceOf(Addresses.class));
        assertThat(response.body().getLatitude(), is(notNullValue()));
        assertThat(response.body().getLongitude(), is(notNullValue()));
        assertThat(response.body().getAddresses(), is(notNullValue()));
    }

    @Test
    public void returnsNotFoundResponseGivenIfPostCodeDoesNotExist() throws IOException {
        final Response<Addresses> response = getAddressAPI.searchAddresses("XX4 04X", apiKey).execute();

        assertThat(response, is(notNullValue()));
        assertThat(response.code(), is(NOT_FOUND.value()));
    }

    @Test
    public void returnsBadRequestGivenIfPostCodeIsInvalid() throws IOException {
        final Response<Addresses> response = getAddressAPI.searchAddresses("XX4 00X", apiKey).execute();

        assertThat(response, is(notNullValue()));
        assertThat(response.code(), is(BAD_REQUEST.value()));
    }


    @Test
    public void returnsUnAuthorizedResponseGivenIfAPIKeyIsNotValid() throws IOException {
        final Response<Addresses> response = getAddressAPI.searchAddresses("XX4 01X", "SOME_INVALID_API_KEY").execute();

        assertThat(response, is(notNullValue()));
        assertThat(response.code(), is(UNAUTHORIZED.value()));
    }
}
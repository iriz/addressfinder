package com.loveholidays.addressfinder.api;

import com.loveholidays.addressfinder.domain.Addresses;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class AddressesTest {

    public static final List<String> RAW_ADDRESSES = asList(
            "16 Watkin Terrace, , , , , Northampton, Northamptonshire",
            "26d Watkin Terrace, , , , , Northampton, Northamptonshire");

    @Test
    public void shouldReturnFormatAddresses(){
        Addresses addresses = new Addresses();
        addresses.setAddresses(RAW_ADDRESSES);

        assertThat(addresses.getAddresses(), is(notNullValue()));
        assertThat(addresses.formattedAddresses(), contains(
                "16 Watkin Terrace, Northampton, Northamptonshire",
                "26d Watkin Terrace, Northampton, Northamptonshire"
        ));
    }
}
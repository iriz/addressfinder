Address Finder
==============

- To build the application execute the `./gradlew build`
- To run the application `./gradlew bootRun`


Trying the app
========

- Run the application `./gradlew bootRun`
- Then use `http://localhost:8080/`


To test the REST controller
=======================

- Run the application `./gradlew bootRun`
- The use the `GET` endpoint `http://localhost:8080/addresses/{POST_CODE}`